var burgerMenu = document.querySelector('.burger-menu');
var navMenu = document.querySelector('.nav-menu');
burgerMenu.addEventListener('click', function (evt) {
    var $this = evt.currentTarget;
    if ($this.classList.contains('active')) {
        $this.classList.remove('active');
        navMenu.classList.remove('active');
    }  else {
        $this.classList.add('active');
        navMenu.classList.add('active');
    }
});

var menuItems = document.querySelectorAll('.menu-list > .item');
for (var i = 0; i < menuItems.length ; i++) {
    menuItems[i].addEventListener('mouseenter', function (evt) {
        var activeSub = document.querySelector('.active-sub');
        var $this = evt.currentTarget;
        var childrens = $this.childNodes;
        var submenu = null;
        for (var j = 0; j < childrens.length ; j++) {
            if (childrens[j].className === 'submenu') {
                submenu = childrens[j];
            }
        }
        if (submenu) {
            if (activeSub) {
                activeSub.classList.remove('active-sub');
            }
            submenu.classList.add('active-sub');
        }
    })
}

/***** Header *****/

const nav = document.querySelector('nav');

window.onscroll = () => {
    let scrollPos = {
        x: window.scrollX,
        y: window.scrollY
    };

    if (scrollPos.y > 100) {
        nav.classList.add('fixed')
    } else {
        nav.classList.remove('fixed')
    }
};
