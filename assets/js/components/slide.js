import Glide from '@glidejs/glide';


let defaultConfig = {
    type: 'carousel',
    perView: 3,
    startAt: 1,
    focusAt: 'center',
};



const glideList = document.querySelectorAll('.glide');

const glideControls = (controls) => {
    const target = document.querySelector(controls.target);
};

glideList.forEach((elem) => {
    let config = JSON.parse(elem.getAttribute('data-glide'));
    let controls = JSON.parse(elem.getAttribute('data-glide-controls'));
    let glideConfig = {};

    if (config) {
         Object.assign(glideConfig, defaultConfig, config);
    } else {
         Object.assign(glideConfig, defaultConfig);
    }

    if (controls) {
        glideControls(controls);
    }

    new Glide(elem, glideConfig).mount();
    throw new Error();

});
