document.addEventListener("load", function() {

});


const $ = require('jquery');

$(document).ready(function(){

    //Init content
    getDishData('menu');

    $('.button-dish').each(function() {

        console.log('clicked');

        //Event listener on buttons
        $(this).on('click', function(event){

            resetSelectedButton();

            $(this).addClass('selected');

            var type         = $(this).data('type');
            var button       = $(this);

            getDishData(type, button);

        });
    })
});

function clearContainer() {
    $('#dishes').empty();
}

function formatRender(restaurantSlug, params) {
    console.log(params);
    var res = `
            <a href="/restaurant/${restaurantSlug}/plat/${params['dishId']}" class="dish-container col-md-4 col-12">
                <div class="image-container">
                    <img class="image" src="/images/dishes/${params['cover_picture']}" alt="${params['name']}"/>
                </div>
                <div class="name">${params['name']}</div>
                <div class="type">${params['dishType']}</div>
                <div class="price">${params['price']} €</div>
            </a>
        `;
    return res;
}

function resetSelectedButton() {
    $('.button-dish').each(function() {
        $(this).removeClass('selected');
    });
}

function getDishData(type, button = null) {
    var loader = document.getElementById('menu-loader');
    var restaurantId    = $('#restaurantId').val();
    var restaurantSlug  = $('#restaurantSlug').val();
    var container       = $('#dishes');

    console.log(loader);

    $.ajax({
        url:        '/ajax/entries',
        type:       'POST',
        dataType:   'json',
        async:      true,
        data:       {
            type:   type,
            slug:     restaurantSlug
        },
        beforeSend: function() {
            //Clear container
            clearContainer();
            //Show loader
            loader.classList.toggle('d-none');
            if(button != null)
                button.prop('disabled', true);
        },
        success: function(res, status) {
            //Parse json to js array
            res = JSON.parse(res).data;
            //Format data and add to html
            if (res.length > 0) {
                for (var i = 0; i < res.length; i++) {
                    container.append(formatRender(restaurantSlug, res[i]));
                }
            } else {
                container.append('Aucun résultat.');
            }
        },
        error: function(xhr, textStatus, errorThrown) {
            console.log('error')
        },
        complete: function() {
            if(button != null)
                button.prop('disabled', false);
            //Hide loader
            loader.classList.toggle('d-none');
        }
    });
}
