<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190212135428 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE allergen CHANGE name name VARCHAR(100) NOT NULL, CHANGE icon icon VARCHAR(100) NOT NULL');
        $this->addSql('ALTER TABLE category CHANGE name name VARCHAR(100) NOT NULL, CHANGE slug slug VARCHAR(100) NOT NULL, CHANGE icon icon VARCHAR(100) NOT NULL');
        $this->addSql('ALTER TABLE dish CHANGE name name VARCHAR(100) NOT NULL');
        $this->addSql('ALTER TABLE dish_type CHANGE label label VARCHAR(100) NOT NULL, CHANGE icon icon VARCHAR(50) NOT NULL');
        $this->addSql('ALTER TABLE grower CHANGE firstname firstname VARCHAR(50) NOT NULL, CHANGE lastname lastname VARCHAR(50) NOT NULL, CHANGE url url VARCHAR(100) DEFAULT NULL, CHANGE city city VARCHAR(100) NOT NULL');
        $this->addSql('ALTER TABLE menu CHANGE name name VARCHAR(50) NOT NULL');
        $this->addSql('ALTER TABLE news CHANGE title title VARCHAR(255) NOT NULL, CHANGE meta_title meta_title VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE product CHANGE name name VARCHAR(100) NOT NULL');
        $this->addSql('ALTER TABLE restaurant CHANGE name name VARCHAR(100) NOT NULL, CHANGE slug slug VARCHAR(255) NOT NULL, CHANGE mail mail VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE user CHANGE username username VARCHAR(100) NOT NULL, CHANGE mail mail VARCHAR(255) NOT NULL, CHANGE password password VARCHAR(255) NOT NULL, CHANGE firstname firstname VARCHAR(100) NOT NULL, CHANGE lastname lastname VARCHAR(100) NOT NULL, CHANGE photo photo VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE allergen CHANGE name name VARCHAR(30) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE icon icon VARCHAR(20) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE category CHANGE name name VARCHAR(50) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE slug slug VARCHAR(70) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE icon icon VARCHAR(50) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE dish CHANGE name name VARCHAR(30) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE dish_type CHANGE label label VARCHAR(30) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE icon icon VARCHAR(20) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE grower CHANGE firstname firstname VARCHAR(30) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE lastname lastname VARCHAR(30) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE url url VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, CHANGE city city VARCHAR(50) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE menu CHANGE name name VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE news CHANGE title title VARCHAR(100) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE meta_title meta_title VARCHAR(100) DEFAULT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE product CHANGE name name VARCHAR(50) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE restaurant CHANGE name name VARCHAR(50) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE slug slug VARCHAR(100) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE mail mail VARCHAR(200) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE user CHANGE username username VARCHAR(30) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE mail mail VARCHAR(100) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE password password VARCHAR(60) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE firstname firstname VARCHAR(30) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE lastname lastname VARCHAR(30) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE photo photo VARCHAR(100) DEFAULT NULL COLLATE utf8mb4_unicode_ci');
    }
}
