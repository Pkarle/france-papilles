<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190617141027 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE news ADD cover_picture VARCHAR(255) NOT NULL, CHANGE title title VARCHAR(255) NOT NULL, CHANGE meta_title meta_title VARCHAR(255) DEFAULT NULL, CHANGE thumbnail thumbnail VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE category CHANGE name name VARCHAR(100) NOT NULL, CHANGE slug slug VARCHAR(100) NOT NULL, CHANGE icon icon VARCHAR(100) NOT NULL');
        $this->addSql('ALTER TABLE product ADD thumbnail VARCHAR(255) DEFAULT NULL, CHANGE name name VARCHAR(100) NOT NULL');
        $this->addSql('ALTER TABLE dish_type CHANGE label label VARCHAR(100) NOT NULL, CHANGE icon icon VARCHAR(50) NOT NULL');
        $this->addSql('ALTER TABLE user CHANGE username username VARCHAR(100) NOT NULL, CHANGE mail mail VARCHAR(255) NOT NULL, CHANGE password password VARCHAR(255) NOT NULL, CHANGE firstname firstname VARCHAR(100) NOT NULL, CHANGE lastname lastname VARCHAR(100) NOT NULL, CHANGE photo photo VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE restaurant ADD image VARCHAR(255) DEFAULT NULL, ADD cover VARCHAR(255) DEFAULT NULL, DROP photos, CHANGE name name VARCHAR(100) NOT NULL, CHANGE slug slug VARCHAR(255) NOT NULL, CHANGE mail mail VARCHAR(255) NOT NULL, CHANGE country country VARCHAR(100) NOT NULL, CHANGE city city VARCHAR(100) NOT NULL');
        $this->addSql('ALTER TABLE menu ADD image VARCHAR(255) DEFAULT NULL, CHANGE name name VARCHAR(50) NOT NULL');
        $this->addSql('ALTER TABLE allergen CHANGE name name VARCHAR(100) NOT NULL, CHANGE icon icon VARCHAR(100) NOT NULL');
        $this->addSql('ALTER TABLE grower ADD cover_picture VARCHAR(255) DEFAULT NULL, CHANGE firstname firstname VARCHAR(50) NOT NULL, CHANGE lastname lastname VARCHAR(50) NOT NULL, CHANGE mail mail VARCHAR(255) DEFAULT NULL, CHANGE country country VARCHAR(100) NOT NULL, CHANGE city city VARCHAR(100) NOT NULL');
        $this->addSql('ALTER TABLE dish ADD cover_picture VARCHAR(255) DEFAULT NULL, CHANGE name name VARCHAR(100) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE allergen CHANGE name name VARCHAR(30) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE icon icon VARCHAR(20) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE category CHANGE name name VARCHAR(50) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE slug slug VARCHAR(70) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE icon icon VARCHAR(50) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE dish DROP cover_picture, CHANGE name name VARCHAR(30) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE dish_type CHANGE label label VARCHAR(30) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE icon icon VARCHAR(20) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE grower DROP cover_picture, CHANGE firstname firstname VARCHAR(30) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE lastname lastname VARCHAR(30) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE mail mail VARCHAR(30) DEFAULT NULL COLLATE utf8mb4_unicode_ci, CHANGE country country VARCHAR(20) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE city city VARCHAR(20) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE menu DROP image, CHANGE name name VARCHAR(30) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE news DROP cover_picture, CHANGE title title VARCHAR(100) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE meta_title meta_title VARCHAR(100) DEFAULT NULL COLLATE utf8mb4_unicode_ci, CHANGE thumbnail thumbnail LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:array)\'');
        $this->addSql('ALTER TABLE product DROP thumbnail, CHANGE name name VARCHAR(50) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE restaurant ADD photos LONGTEXT DEFAULT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:array)\', DROP image, DROP cover, CHANGE name name VARCHAR(50) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE slug slug VARCHAR(100) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE mail mail VARCHAR(30) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE country country VARCHAR(20) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE city city VARCHAR(20) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE user CHANGE username username VARCHAR(30) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE mail mail VARCHAR(100) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE password password VARCHAR(60) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE firstname firstname VARCHAR(30) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE lastname lastname VARCHAR(30) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE photo photo VARCHAR(100) DEFAULT NULL COLLATE utf8mb4_unicode_ci');
    }
}
