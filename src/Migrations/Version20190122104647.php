<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190122104647 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE allergen (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(30) NOT NULL, icon VARCHAR(20) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE category (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, slug VARCHAR(70) NOT NULL, description LONGTEXT DEFAULT NULL, photo VARCHAR(255) NOT NULL, icon VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE category_news (category_id INT NOT NULL, news_id INT NOT NULL, INDEX IDX_9648E31712469DE2 (category_id), INDEX IDX_9648E317B5A459A0 (news_id), PRIMARY KEY(category_id, news_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dish (id INT AUTO_INCREMENT NOT NULL, dish_type_id INT DEFAULT NULL, name VARCHAR(30) NOT NULL, description LONGTEXT DEFAULT NULL, photos LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', price DOUBLE PRECISION NOT NULL, headline TINYINT(1) NOT NULL, INDEX IDX_957D8CB855FB9605 (dish_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dish_allergen (dish_id INT NOT NULL, allergen_id INT NOT NULL, INDEX IDX_3C4389A5148EB0CB (dish_id), INDEX IDX_3C4389A56E775A4A (allergen_id), PRIMARY KEY(dish_id, allergen_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dish_menu (dish_id INT NOT NULL, menu_id INT NOT NULL, INDEX IDX_F7078582148EB0CB (dish_id), INDEX IDX_F7078582CCD7E912 (menu_id), PRIMARY KEY(dish_id, menu_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dish_type (id INT AUTO_INCREMENT NOT NULL, label VARCHAR(30) NOT NULL, icon VARCHAR(20) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE grower (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) NOT NULL, firstname VARCHAR(30) NOT NULL, lastname VARCHAR(30) NOT NULL, description LONGTEXT DEFAULT NULL, photos LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', url VARCHAR(100) DEFAULT NULL, phone VARCHAR(20) NOT NULL, mail VARCHAR(30) DEFAULT NULL, country VARCHAR(20) NOT NULL, city VARCHAR(20) NOT NULL, postal_code VARCHAR(20) NOT NULL, street_number VARCHAR(20) NOT NULL, lattitude DOUBLE PRECISION NOT NULL, longitude DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE menu (id INT AUTO_INCREMENT NOT NULL, restaurant_id INT NOT NULL, name VARCHAR(30) NOT NULL, description LONGTEXT NOT NULL, photos LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', price DOUBLE PRECISION NOT NULL, headline TINYINT(1) NOT NULL, INDEX IDX_7D053A93B1E7706E (restaurant_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE news (id INT AUTO_INCREMENT NOT NULL, slug VARCHAR(100) NOT NULL, title VARCHAR(100) NOT NULL, content LONGTEXT NOT NULL, creation DATETIME NOT NULL, modified DATETIME NOT NULL, meta_description LONGTEXT DEFAULT NULL, meta_title VARCHAR(100) DEFAULT NULL, thumbnail LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', status TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE news_restaurant (news_id INT NOT NULL, restaurant_id INT NOT NULL, INDEX IDX_7EAC546B5A459A0 (news_id), INDEX IDX_7EAC546B1E7706E (restaurant_id), PRIMARY KEY(news_id, restaurant_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product (id INT AUTO_INCREMENT NOT NULL, grower_id INT DEFAULT NULL, name VARCHAR(50) NOT NULL, description LONGTEXT DEFAULT NULL, photos LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', headline TINYINT(1) NOT NULL, INDEX IDX_D34A04AD5243E353 (grower_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_dish (product_id INT NOT NULL, dish_id INT NOT NULL, INDEX IDX_1895AE194584665A (product_id), INDEX IDX_1895AE19148EB0CB (dish_id), PRIMARY KEY(product_id, dish_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE restaurant (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, slug VARCHAR(100) NOT NULL, description LONGTEXT NOT NULL, photos LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', phone VARCHAR(20) NOT NULL, mail VARCHAR(30) NOT NULL, country VARCHAR(20) NOT NULL, city VARCHAR(20) NOT NULL, postal_code VARCHAR(20) NOT NULL, street_number VARCHAR(20) NOT NULL, lattitude DOUBLE PRECISION NOT NULL, longitude DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE restaurant_product (restaurant_id INT NOT NULL, product_id INT NOT NULL, INDEX IDX_190158D8B1E7706E (restaurant_id), INDEX IDX_190158D84584665A (product_id), PRIMARY KEY(restaurant_id, product_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE restaurant_user (restaurant_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_4F85462DB1E7706E (restaurant_id), INDEX IDX_4F85462DA76ED395 (user_id), PRIMARY KEY(restaurant_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(30) NOT NULL, mail VARCHAR(100) NOT NULL, password VARCHAR(60) NOT NULL, firstname VARCHAR(30) NOT NULL, lastname VARCHAR(30) NOT NULL, photo VARCHAR(100) DEFAULT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE category_news ADD CONSTRAINT FK_9648E31712469DE2 FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE category_news ADD CONSTRAINT FK_9648E317B5A459A0 FOREIGN KEY (news_id) REFERENCES news (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE dish ADD CONSTRAINT FK_957D8CB855FB9605 FOREIGN KEY (dish_type_id) REFERENCES dish_type (id)');
        $this->addSql('ALTER TABLE dish_allergen ADD CONSTRAINT FK_3C4389A5148EB0CB FOREIGN KEY (dish_id) REFERENCES dish (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE dish_allergen ADD CONSTRAINT FK_3C4389A56E775A4A FOREIGN KEY (allergen_id) REFERENCES allergen (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE dish_menu ADD CONSTRAINT FK_F7078582148EB0CB FOREIGN KEY (dish_id) REFERENCES dish (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE dish_menu ADD CONSTRAINT FK_F7078582CCD7E912 FOREIGN KEY (menu_id) REFERENCES menu (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE menu ADD CONSTRAINT FK_7D053A93B1E7706E FOREIGN KEY (restaurant_id) REFERENCES restaurant (id)');
        $this->addSql('ALTER TABLE news_restaurant ADD CONSTRAINT FK_7EAC546B5A459A0 FOREIGN KEY (news_id) REFERENCES news (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE news_restaurant ADD CONSTRAINT FK_7EAC546B1E7706E FOREIGN KEY (restaurant_id) REFERENCES restaurant (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD5243E353 FOREIGN KEY (grower_id) REFERENCES grower (id)');
        $this->addSql('ALTER TABLE product_dish ADD CONSTRAINT FK_1895AE194584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product_dish ADD CONSTRAINT FK_1895AE19148EB0CB FOREIGN KEY (dish_id) REFERENCES dish (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE restaurant_product ADD CONSTRAINT FK_190158D8B1E7706E FOREIGN KEY (restaurant_id) REFERENCES restaurant (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE restaurant_product ADD CONSTRAINT FK_190158D84584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE restaurant_user ADD CONSTRAINT FK_4F85462DB1E7706E FOREIGN KEY (restaurant_id) REFERENCES restaurant (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE restaurant_user ADD CONSTRAINT FK_4F85462DA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE dish_allergen DROP FOREIGN KEY FK_3C4389A56E775A4A');
        $this->addSql('ALTER TABLE category_news DROP FOREIGN KEY FK_9648E31712469DE2');
        $this->addSql('ALTER TABLE dish_allergen DROP FOREIGN KEY FK_3C4389A5148EB0CB');
        $this->addSql('ALTER TABLE dish_menu DROP FOREIGN KEY FK_F7078582148EB0CB');
        $this->addSql('ALTER TABLE product_dish DROP FOREIGN KEY FK_1895AE19148EB0CB');
        $this->addSql('ALTER TABLE dish DROP FOREIGN KEY FK_957D8CB855FB9605');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD5243E353');
        $this->addSql('ALTER TABLE dish_menu DROP FOREIGN KEY FK_F7078582CCD7E912');
        $this->addSql('ALTER TABLE category_news DROP FOREIGN KEY FK_9648E317B5A459A0');
        $this->addSql('ALTER TABLE news_restaurant DROP FOREIGN KEY FK_7EAC546B5A459A0');
        $this->addSql('ALTER TABLE product_dish DROP FOREIGN KEY FK_1895AE194584665A');
        $this->addSql('ALTER TABLE restaurant_product DROP FOREIGN KEY FK_190158D84584665A');
        $this->addSql('ALTER TABLE menu DROP FOREIGN KEY FK_7D053A93B1E7706E');
        $this->addSql('ALTER TABLE news_restaurant DROP FOREIGN KEY FK_7EAC546B1E7706E');
        $this->addSql('ALTER TABLE restaurant_product DROP FOREIGN KEY FK_190158D8B1E7706E');
        $this->addSql('ALTER TABLE restaurant_user DROP FOREIGN KEY FK_4F85462DB1E7706E');
        $this->addSql('ALTER TABLE restaurant_user DROP FOREIGN KEY FK_4F85462DA76ED395');
        $this->addSql('DROP TABLE allergen');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE category_news');
        $this->addSql('DROP TABLE dish');
        $this->addSql('DROP TABLE dish_allergen');
        $this->addSql('DROP TABLE dish_menu');
        $this->addSql('DROP TABLE dish_type');
        $this->addSql('DROP TABLE grower');
        $this->addSql('DROP TABLE menu');
        $this->addSql('DROP TABLE news');
        $this->addSql('DROP TABLE news_restaurant');
        $this->addSql('DROP TABLE product');
        $this->addSql('DROP TABLE product_dish');
        $this->addSql('DROP TABLE restaurant');
        $this->addSql('DROP TABLE restaurant_product');
        $this->addSql('DROP TABLE restaurant_user');
        $this->addSql('DROP TABLE user');
    }
}
