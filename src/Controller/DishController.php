<?php

namespace App\Controller;

use App\Entity\Dish;
use App\Entity\Restaurant;
use App\Repository\DishRepository;
use App\Repository\RestaurantRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DishController extends AbstractController {

    /**
     * @Route("restaurant/{restaurant_slug}/plat/{id}", name="plat")
     * @param $restaurant_slug
     * @param Dish $dish
     * @param DishRepository $dishRepository
     * @param RestaurantRepository $restoRepository
     * @return Response
     */
    public function dish($restaurant_slug, Dish $dish, DishRepository $dishRepository, RestaurantRepository $restoRepository) {

        $linkedDishes = $dishRepository->findRestaurantDishByType(
            $restaurant_slug,
            $dish->getDishType()->getTrigram(),
            $dish->getId(),
            4
        );

        $restaurant = $restoRepository->findOneBy(['slug' => $restaurant_slug]);

        return $this->render('pages/dish.html.twig', [
            'dish'          => $dish,
            'linkedDishes'  => $linkedDishes,
            'restaurant'    => $restaurant
        ]);
    }

}