<?php

namespace App\Controller;

use App\Entity\Restaurant;
use App\Repository\NewsRepository;
use App\Repository\RestaurantRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class NewsController extends AbstractController
{
    /**
     * @Route("/restaurant/{restaurant_slug}/journal", name="journal_listing")
     */
    public function index(string $restaurant_slug, NewsRepository $repository, RestaurantRepository $restaurantRepository)
    {
        $data['restaurant'] = $restaurantRepository->findOneBy(['slug' => $restaurant_slug]);
        $data['posts'] = $repository->findByRestaurantId($data['restaurant']->getId());

        return $this->render('new/index.html.twig', $data);
    }
}
