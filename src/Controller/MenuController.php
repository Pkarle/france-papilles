<?php

namespace App\Controller;

use App\Entity\Restaurant;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MenuController extends AbstractController {

    /**
     * @Route("restaurant/{slug}/carte   ", name="menu")
     * @param Restaurant $restaurant
     * @return Response
     */
    public function view(Restaurant $restaurant) {
        return $this->render('pages/menu.html.twig', [
            'restaurant'    => $restaurant
        ]);
    }
}