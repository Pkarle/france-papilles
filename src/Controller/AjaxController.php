<?php

namespace App\Controller;

use App\Entity\Restaurant;
use App\Repository\DishRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AjaxController extends AbstractController {

    /**
     * @Route("/ajax/entries")
     * @return JsonResponse
     */
    public function getEntries(DishRepository $dishRepository) {

        $entries = $dishRepository->findRestaurantDishByType($_POST['slug'], $_POST['type']);

        $jsonData = json_encode([
            'data'   => $entries
        ]);
        return new JsonResponse($jsonData);
    }

}