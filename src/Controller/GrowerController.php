<?php

namespace App\Controller;

use App\Entity\Grower;
use App\Repository\GrowerRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class GrowerController extends AbstractController {

    /**
     * @Route("/producteur/{id}/", name="producteur")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function grower(GrowerRepository $growerRepository, Grower $grower) {

        return $this->render('pages/grower.html.twig', [
            'grower'  => $grower
        ]);
    }

}