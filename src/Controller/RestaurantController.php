<?php
/**
 * Created by PhpStorm.
 * User: Pierr
 * Date: 02/04/2019
 * Time: 10:23
 */

namespace App\Controller;


use App\Entity\Restaurant;
use App\Repository\RestaurantRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RestaurantController extends AbstractController {

    /**
     * @Route("/restaurant/{slug}", name="restaurant")
     * @param RestaurantRepository $restaurantRepository
     * @param Restaurant $restaurant
     * @return Response
     */
    public function restaurant(RestaurantRepository $restaurantRepository, Restaurant $restaurant) {

        $dishes = $restaurantRepository->findRestaurantDishes($restaurant, true);
        $news   = $restaurantRepository->findRestaurantNews($restaurant, 3);

        return $this->render('pages/restaurant.html.twig', [
            'restaurant'    => $restaurant,
            'dishes'        => $dishes,
            'news'          => $news
        ]);
    }
}