<?php

namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DishRepository")
 * @Vich\Uploadable
 */
class Dish
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $cover_picture;

    /**
     * @Vich\UploadableField(mapping="dish_images", fileNameProperty="cover_picture")
     * @var File
     */
    private $coverPictureFile;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $picture;

    /**
     * @Vich\UploadableField(mapping="dish_images", fileNameProperty="picture")
     * @var File
     */
    private $pictureFile;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $picture2;

    /**
     * @Vich\UploadableField(mapping="dish_images", fileNameProperty="picture2")
     * @var File
     */
    private $pictureFile2;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $picture3;

    /**
     * @Vich\UploadableField(mapping="dish_images", fileNameProperty="picture3")
     * @var File
     */
    private $pictureFile3;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $photos = [];

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\Column(type="boolean")
     */
    private $headline;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Product", mappedBy="dishes", fetch="EAGER")
     */
    private $products;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Allergen", inversedBy="dishes", fetch="EAGER")
     */
    private $allergens;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Menu", inversedBy="dishes")
     */
    private $menus;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\DishType", inversedBy="dishes")
     */
    private $dishType;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Restaurant", mappedBy="dishes", fetch="EAGER")
     */
    private $restaurants;

    /**
     * @ORM\Column(type="datetime")
     * @var DateTime
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     * @var DateTime
     */
    private $updatedAt;

    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->allergens = new ArrayCollection();
        $this->menus = new ArrayCollection();
        $this->restaurants = new ArrayCollection();

        $this->setCreatedAt(new DateTime());
        $this->setUpdatedAt(new DateTime());
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPhotos(): ?array
    {
        return $this->photos;
    }

    public function setPhotos(?array $photos): self
    {
        $this->photos = $photos;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getHeadline(): ?bool
    {
        return $this->headline;
    }

    public function setHeadline(bool $headline): self
    {
        $this->headline = $headline;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->addDish($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->contains($product)) {
            $this->products->removeElement($product);
            $product->removeDish($this);
        }

        return $this;
    }

    /**
     * @return Collection|Allergen[]
     */
    public function getAllergens(): Collection
    {
        return $this->allergens;
    }

    public function addAllergen(Allergen $allergen): self
    {
        if (!$this->allergens->contains($allergen)) {
            $this->allergens[] = $allergen;
        }

        return $this;
    }

    public function removeAllergen(Allergen $allergen): self
    {
        if ($this->allergens->contains($allergen)) {
            $this->allergens->removeElement($allergen);
        }

        return $this;
    }

    /**
     * @return Collection|Menu[]
     */
    public function getMenus(): Collection
    {
        return $this->menus;
    }

    public function addMenu(Menu $menu): self
    {
        if (!$this->menus->contains($menu)) {
            $this->menus[] = $menu;
        }

        return $this;
    }

    public function removeMenu(Menu $menu): self
    {
        if ($this->menus->contains($menu)) {
            $this->menus->removeElement($menu);
        }

        return $this;
    }

    public function getDishType(): ?DishType
    {
        return $this->dishType;
    }

    public function setDishType(?DishType $dishType): self
    {
        $this->dishType = $dishType;

        return $this;
    }

    /**
     * @return Collection|Restaurant[]
     */
    public function getRestaurants(): Collection
    {
        return $this->restaurants;
    }

    public function addRestaurant(Restaurant $restaurant): self
    {
        if (!$this->restaurants->contains($restaurant)) {
            $this->restaurants[] = $restaurant;
            $restaurant->addDish($this);
        }

        return $this;
    }

    public function removeRestaurant(Restaurant $restaurant): self
    {
        if ($this->restaurants->contains($restaurant)) {
            $this->restaurants->removeElement($restaurant);
            $restaurant->removeDish($this);
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCoverPicture()
    {
        return $this->cover_picture;
    }

    /**
     * @param mixed $cover_picture
     */
    public function setCoverPicture($cover_picture): void
    {
        $this->cover_picture = $cover_picture;
    }

    /**
     * Generates the magic method
     *
     */
    public function __toString()
    {
        // to show the name of the Category in the select
        return $this->name;
        // to show the id of the Category in the select
        // return $this->id;
    }

    /**
     * @return File
     */
    public function getCoverPictureFile()
    {
        return $this->coverPictureFile;
    }

    /**
     * @param $image
     * @throws \Exception
     */
    public function setCoverPictureFile($image)
    {
        $this->coverPictureFile = $image;

        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     */
    public function setCreatedAt(DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt(): DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTime $updatedAt
     */
    public function setUpdatedAt(DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return mixed
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @param mixed $picture
     */
    public function setPicture($picture): void
    {
        $this->picture = $picture;
    }

    /**
     * @return File
     */
    public function getPictureFile()
    {
        return $this->pictureFile;
    }

    /**
     * @param $image
     * @throws \Exception
     */
    public function setPictureFile($image)
    {
        $this->pictureFile = $image;

        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return mixed
     */
    public function getPicture2()
    {
        return $this->picture2;
    }

    /**
     * @param mixed $picture2
     */
    public function setPicture2($picture2): void
    {
        $this->picture2 = $picture2;
    }

    /**
     * @return File
     */
    public function getPictureFile2()
    {
        return $this->pictureFile2;
    }

    /**
     * @param $image
     * @throws \Exception
     */
    public function setPictureFile2($image)
    {
        $this->pictureFile2 = $image;

        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return mixed
     */
    public function getPicture3()
    {
        return $this->picture3;
    }

    /**
     * @param mixed $picture3
     */
    public function setPicture3($picture3): void
    {
        $this->picture3 = $picture3;
    }

    /**
     * @return File
     */
    public function getPictureFile3()
    {
        return $this->pictureFile3;
    }

    /**
     * @param $image
     * @throws \Exception
     */
    public function setPictureFile3($image)
    {
        $this->pictureFile3 = $image;

        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }
}
