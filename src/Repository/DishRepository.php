<?php

namespace App\Repository;

use App\Entity\Dish;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Dish|null find($id, $lockMode = null, $lockVersion = null)
 * @method Dish|null findOneBy(array $criteria, array $orderBy = null)
 * @method Dish[]    findAll()
 * @method Dish[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DishRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Dish::class);
    }

    public function findRestaurantDishByType($restaurantSlug, $dishType, $current = 'null', $limit = null) {
        $qb = $this->createQueryBuilder('d');
        $qb->select('d.id as dishId, d.name, d.price, dt.label as dishType, r.id as restaurantId, d.photos, d.cover_picture');
        $qb->join('d.dishType', 'dt');
        $qb->join('d.restaurants', 'r');
        $qb->where('dt.trigram = :type');
        $qb->where('r.slug = :slug');
        $qb->andWhere('dt.trigram = :type');
        $qb->andWhere('d.id != :current');

        $qb->setParameters([
            'slug'      => $restaurantSlug,
            'type'      => $dishType,
            'current'   => $current
        ]);


        if($limit) {
            $qb->setMaxResults($limit);
        }

        return $qb->getQuery()->getResult();
    }

    // /**
    //  * @return Dish[] Returns an array of Dish objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Dish
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
