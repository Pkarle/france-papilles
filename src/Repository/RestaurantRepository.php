<?php

namespace App\Repository;

use App\Entity\Restaurant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use phpDocumentor\Reflection\Types\Boolean;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Restaurant|null find($id, $lockMode = null, $lockVersion = null)
 * @method Restaurant|null findOneBy(array $criteria, array $orderBy = null)
 * @method Restaurant[]    findAll()
 * @method Restaurant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RestaurantRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Restaurant::class);
    }

    /**
     * Get simple restaurant details
     * @return mixed
     */
    public function findAllSimple() {
        $qb = $this->createQueryBuilder('r');
        $qb->select('r.id, r.name, r.slug, r.image, r.cover');
        $qb->orderBy('r.id');
        return $qb->getQuery()->getResult();
    }

    /**
     * Get all dishes of one restaurant
     * @param Restaurant $restaurant
     * @param $headline
     * @return mixed
     */
    public function findRestaurantDishes(Restaurant $restaurant, $headline) {
        $qb = $this->createQueryBuilder('r');
        $qb->join('r.dishes', 'd');
        $qb->where('r.id = ' . $restaurant->getId());
        if($headline) {
            $qb->andWhere('d.headline = 1');
        }
        $qb->select('distinct d.id, d.name, d.photos, d.price, d.cover_picture');
        return $qb->getQuery()->getResult();
    }

    /**
     * Get all new of one restaurant
     * @param Restaurant $restaurant
     * @return mixed
     */
    public function findRestaurantNews(Restaurant $restaurant, $number = null) {
        $qb = $this->createQueryBuilder('r');
        $qb->select('n.id, n.slug, n.title, n.content, n.creation, n.thumbnail');
        $qb->join('r.news', 'n');
        $qb->where('r.id = ' . $restaurant->getId());
        $qb->andWhere('n.status = 1');
        if($number) {
            $qb->setMaxResults($number);
        }
        $qb->orderBy('n.creation', 'desc');
        return $qb->getQuery()->getResult();
    }
}
