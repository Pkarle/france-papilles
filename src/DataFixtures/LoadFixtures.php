<?php

namespace App\DataFixtures;

use App\Entity\Allergen;
use App\Entity\Category;
use App\Entity\Dish;
use App\Entity\DishType;
use App\Entity\Grower;
use App\Entity\Menu;
use App\Entity\News;
use App\Entity\Product;
use App\Entity\Restaurant;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;


class LoadFixtures extends Fixture{

    public function load(ObjectManager $manager) {

        //php bin/console doctrine:fixtures:load
        //ini_set('memory_limit', '-1');

        $foodName = [
            'Pizza au fromage', 'Hamburger', 'Cheeseburger', 'Moules Marinières', 'Salade Grecque',
            'Petit Hamburger', 'Petit Cheeseburger', 'Petit Bacon Burger', 'Petit Bacon Cheeseburger',
            'Sandwich Vegan', 'Sandwich Vegan au fromage', 'Fromage grillé', 'Pates',
            'Cheese Dog', 'Hot Dog', 'Welsh', 'Tartiflette', 'Boeuf Bourguignons'
        ];

        $restaurantsData = [
            1 => [
                'name' => 'Annecy Papilles',
                'city' => 'Annecy',
                'slug' => 'restaurant-annecy-papilles'
            ],
            2 => [
                'name' => 'Biarritz Papilles',
                'city' => 'Biarritz',
                'slug' => 'restaurant-biarritz-papilles'
            ],
            3 => [
                'name' => 'Lille Papilles',
                'city' => 'Lille',
                'slug' => 'restaurant-lille-papilles'
            ],
            4 => [
                'name' => 'Lyon Papilles',
                'city' => 'Lyon',
                'slug' => 'restaurant-lyon-papilles'
            ],
            5 => [
                'name' => 'Nantes Papilles',
                'city' => 'Nantes',
                'slug' => 'restaurant-nantes-papilles'
            ]
        ];
        $restaurants = [];

        //Growers
        $growers = [];
        for($i = 0; $i <= 5; $i++) {
            $grower = $this->_createGrowers();
            $growers[] = $grower;
            $manager->persist($grower);
        }
        $manager->flush();

        //DishType
        $dishTypes = [];
        for($i = 0; $i <= 5; $i++) {
            $dishType = $this->_createDishType($foodName);
            $dishTypes[] = $dishType;
            $manager->persist($dishType);
        }
        $manager->flush();

        //Allergens
        $allergens = [];
        for($i = 0; $i <= 5; $i++) {
            $allergen = $this->_createAllergen($foodName);
            $allergens[] = $allergen;
            $manager->persist($allergen);
        }
        $manager->flush();

        //Dish
        $dishes = [];
        for($i = 0; $i <= 5; $i++) {
            $dish = $this->_createDish($foodName);
            $dishes[] = $dish;
            $dish->setDishType($dishTypes[mt_rand(1, count($dishTypes) - 1)]);
            for($i = 0; $i <= mt_rand(1, 5); $i++) {
                $dish->addAllergen($allergens[mt_rand(1, count($allergens) - 1)]);
            }
            $dishes[] = $dish;
            $manager->persist($dish);
        }
        $manager->flush();

        //User global
        $globalUser = new User();

        $globalUser
            ->setFirstname("Admin")
            ->setLastname("Admin")
            ->setPassword("admin")
            ->setUsername("admin")
            ->setMail("admin@gmail.com")
            ->setRoles([]);
        $manager->flush();

        foreach($restaurantsData as $data) {

            //restaurant
            $restaurant = $this->_createRestaurant($data);
            $restaurants[] = $restaurant;
            $manager->persist($restaurant);
            $manager->flush();

            //global user
            $globalUser->addRestaurant($restaurant);
            $manager->persist($globalUser);
            $manager->flush();

            //simple user
            $manager->persist($this->_createUser($restaurant));
            $manager->flush();
        }

        //Menu
        for($i = 0; $i <= 5; $i++) {
            $menu = $this->_createMenu($foodName);
            //Dish
            $menu->addDish($dishes[mt_rand(1, count($dishes)) - 1]);
            //Menu
            $menu->setRestaurant($restaurants[mt_rand(1, count($restaurants)) - 1]);

            //restaurant
            $manager->persist($menu);
        }

        //Products
        $products = [];
        for($i = 0; $i <= 5; $i++) {
            $product = $this->_createProduct($foodName);
            $products[] = $product;
            $product->setGrower($growers[mt_rand(1, count($growers) - 1)]);
            $product->addDish($dishes[mt_rand(1, count($dishes) - 1)]);
            $product->addRestaurant($restaurants[mt_rand(1, count($restaurants) - 1)]);
            $manager->persist($product);
        }
        $manager->flush();

        //Categories
        $categories = [];
        for($i = 0; $i <= 3; $i++) {
            $category = $this->_createCategory();
            $categories[] = $category;
            $manager->persist($category);
        }

        //News
        for($j = 0; $j <= mt_rand(4,5); $j++) {
            $news = $this->_createNews();
            $manager->persist($news);
            $news->addRestaurant($restaurants[mt_rand(1, count($restaurants) - 1)]);
            $news->addCategory($categories[mt_rand(1, count($categories) - 1)]);
        }

        $manager->flush();
    }

    private function _createRestaurant($data) {

        $faker = \Faker\Factory::create('fr_FR');

        $restaurant = new Restaurant();
        $restaurant
            ->setName($data['name'])
            ->setSlug($data['slug'])
            ->setDescription($faker->paragraph())
            ->setPhotos([])
            ->setPhone($faker->phoneNumber())
            ->setMail($faker->email())
            ->setCountry('France')
            ->setCity($data['city'])
            ->setPostalCode($faker->postcode())
            ->setStreetNumber(rand(1, 20))
            ->setLattitude($faker->latitude())
            ->setLongitude($faker->longitude());

        return $restaurant;
    }

    private function _createUser(Restaurant $restaurant) {

        $faker = \Faker\Factory::create('fr_FR');

        //simple user
        $user = new User();
        $user
            ->setFirstname($faker->firstName())
            ->setLastname($faker->lastName())
            ->setPassword($faker->password())
            ->setUsername($faker->userName())
            ->setMail($faker->email())
            ->setRoles([])
            ->addRestaurant($restaurant);

        return $user;
    }

    private function _createNews()
    {

        $faker = \Faker\Factory::create('fr_FR');

        $content = '<p>' . join($faker->paragraphs(mt_rand(3, 10)), '</p><p>') . '</p>';
        $date = $faker->dateTimeBetween('-6 months');
        $news = new News();
        $news
            ->setTitle($faker->sentence())
            ->setSlug($faker->slug(5))
            ->setContent($content)
            ->setThumbnail([$faker->imageUrl()])
            ->setCreation($date)
            ->setModified($date)
            ->setStatus($faker->boolean(70));

        return $news;
    }

    private function _createCategory()
    {

        $faker = \Faker\Factory::create('fr_FR');

        $category = new Category();
        $category
            ->setName($faker->sentence(2))
            ->setSlug($faker->slug(3))
            ->setIcon('')
            ->setPhoto('')
            ->setDescription($faker->paragraph());

        return $category;
    }

    public function _createMenu($foodName) {
        $faker = \Faker\Factory::create('fr_FR');

        $content = '<p>' . join($faker->paragraphs(mt_rand(3, 10)), '</p><p>') . '</p>';
        $menu = new Menu();
        $menu
            ->setDescription($content)
            ->setName("Menu - " . $foodName[array_rand($foodName, 1)])
            ->setPrice($faker->randomNumber(2))
            ->setHeadline($faker->boolean(50));

        return $menu;
    }

    private function _createDish($foodName) {
        $faker = \Faker\Factory::create('fr_FR');

        $content = '<p>' . join($faker->paragraphs(mt_rand(3, 10)), '</p><p>') . '</p>';
        $dish = new Dish();
        $dish
            ->setName($foodName[array_rand($foodName, 1)])
            ->setDescription($content)
            ->setPrice($faker->randomFloat(2, 10, 100))
            ->setHeadline($faker->boolean(50));

        return $dish;
    }

    private function _createAllergen($foodName) {

        $faker = \Faker\Factory::create('fr_FR');

        $allergen = new Allergen();
        $allergen
            ->setName($foodName[array_rand($foodName, 1)])
            ->setIcon("");
        return $allergen;
    }

    private function _createDishType($foodName) {

        $faker = \Faker\Factory::create('fr_FR');

        $dishType = new DishType();
        $dishType
            ->setLabel($foodName[array_rand($foodName, 1)])
            ->setIcon("");

        return $dishType;
    }

    private function _createGrowers() {

        $faker = \Faker\Factory::create('fr_FR');

        $grower = new Grower();
        $content = '<p>' . join($faker->paragraphs(mt_rand(3, 10)), '</p><p>') . '</p>';
        $grower
            ->setName($faker->userName())
            ->setLastname($faker->lastName())
            ->setFirstname($faker->firstName())
            ->setDescription($content)
            ->setMail($faker->email())
            ->setLongitude($faker->longitude())
            ->setLattitude($faker->latitude())
            ->setPostalCode($faker->postcode())
            ->setCity($faker->city())
            ->setCountry("France")
            ->setPhone($faker->phoneNumber())
            ->setStreetNumber(rand(1, 20))
            ->setUrl($faker->url());

        return $grower;
    }

    private function _createProduct($foodName) {
        $faker = \Faker\Factory::create('fr_FR');

        $product = new Product();
        $content = '<p>' . join($faker->paragraphs(mt_rand(1, 3)), '</p><p>') . '</p>';

        $product
            ->setName($foodName[array_rand($foodName, 1)])
            ->setDescription($content)
            ->setHeadline($faker->boolean(20));

        return $product;

    }

}