const Encore = require('@symfony/webpack-encore');
const LiveReloadPlugin = require('webpack-livereload-plugin');

Encore
    // directory where compiled assets will be stored
    .setOutputPath('./public/assets/')
    // public path used by the web server to access the output path
    .setPublicPath('/assets')
    // only needed for CDN's or sub-directory deploy
    //.setManifestKeyPrefix('build/')
    .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())
    // enables hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())
    .enablePostCssLoader((options) => {
        options.config = {
            path: 'postcss.config.js'
        };
    })
    /*
     * ENTRY CONFIG
     *
     * Add 1 entry for each "page" of your app
     * (including one that's included on every page - e.g. "app")
     *
     * Each entry will result in one JavaScript file (e.g. app.js)
     * and one CSS file (e.g. app.css) if you JavaScript imports CSS.
     */
    .addEntry('app', './assets/js/app.js')
    .addEntry('components/menuAjax', './assets/js/menuAjax.js')
    .addEntry('theme', './assets/scss/theme.scss')

    // Components js
    .addEntry('menu', './assets/js/components/menu.js')
    .addEntry('components/slide', './assets/js/components/slide.js')
    //.addEntry('theme', './assets/js/theme.scss')
    // will require an extra script tag for runtime.js
    // but, you probably want this, unless you're building a single-page app

    .addPlugin(new LiveReloadPlugin({
        watch: [
            './public/assets/theme.css'
        ],
    }))

    .enableSingleRuntimeChunk()

    /*
     * FEATURE CONFIG
     *
     * Enable & configure other features below. For a full
     * list of features, see:
     * https://symfony.com/doc/current/frontend.html#adding-more-features
     */

    // enables Sass/SCSS support
    .enableSassLoader()

    // uncomment if you use TypeScript
    //.enableTypeScriptLoader()

    // uncomment if you're having problems with a jQuery plugin
    .autoProvidejQuery()

    // uncomment if you use API Platform Admin (composer req api-admin)
    //.enableReactPreset()
;

module.exports = Encore.getWebpackConfig();
